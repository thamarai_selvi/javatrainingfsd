package com.greatLearning.assignment;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TechDepartment td=new TechDepartment();
		System.out.println("Department Name: "+ td.departmentName());
		System.out.println("Todays Work: "+ td.getTodaysWork());
		System.out.println("Stack Information: "+ td.getTechStackInformation());
		System.out.println("Work Deadline: "+ td.getWorkDeadline());
		System.out.println("Is today a Holiday: "+ td.isTodayAHoliday());

		System.out.println();
		AdminDepartment ad= new AdminDepartment();
		System.out.println("Department Name: "+ad.departmentName());
		System.out.println("Todays Work: "+ ad.getTodaysWork());
		System.out.println("Work Deadline: "+ ad.getWorkDeadline());
		System.out.println("Is today a Holiday: "+ ad.isTodayAHoliday());

		System.out.println();
		HrDepartment hd=new HrDepartment();
		System.out.println("Department Name: "+ hd.departmentName());
		System.out.println("Todays Work: "+ hd.getTodaysWork());
		System.out.println("Activity: "+ hd.doActivity());
		System.out.println("Work Deadline: "+hd.getWorkDeadline());
		System.out.println("Is today a Holiday: "+ hd.isTodayAHoliday());

	
	}

}
