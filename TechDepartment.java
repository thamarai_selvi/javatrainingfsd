package com.greatLearning.assignment;

public class TechDepartment extends SuperDepartment{

	public String departmentName()
	{
		return "Tech Department";
	}
	public String getTodaysWork()
	{
		return"Complete Coding of Module 1";
		
	}
	public String getTechStackInformation()
	{
		return "core Java";
	}
	public String getWorkDeadline()
	{
		return "Complete by EOD";
	}

}
